This style guide documents guidelines and recommendations for building JSON APIs in Trimble CEC. In general, JSON APIs should follow the spec found at JSON.org. This style guide clarifies and standardizes specific cases so that JSON APIs from Trimble CEC have a standard look and feel. These guidelines are applicable to JSON requests and responses in both RPC-based and REST-based APIs. 

------------

[TOC]



### General Guidelines

#### Comments

*No comments in JSON objects.*

Comments should not be included in JSON objects. Some of the examples in this style guide include comments. However this is only to clarify the examples.

```javascript
{
  // You may see comments in the examples below,
  // But don't include comments in your JSON.
  "propertyName": "propertyValue"
}
```

#### Double Quotes vs Single Quotes

*Use double quotes.*

If a property requires quotes, double quotes must be used. All property names must be surrounded by double quotes. Property values of type string must be surrounded by double quotes. Other value types (like boolean or number) should not be surrounded by double quotes.

#### Flattened data vs Structured Hierarchy

*Favor flat structures over hierarchical structures.*

Data elements should be "flattened" in the JSON representation. Data should not be arbitrarily grouped for convenience.

In some cases, such as a collection of properties that represents a single structure, it may make sense to keep the structured hierarchy. These cases should be carefully considered, and only used if it makes semantic sense. For example, an address could be represented two ways, but the structured way probably makes more sense for developers:

Flattened Address:

```json
{
  "company": "Google",
  "website": "http://www.google.com/",
  "addressLine1": "111 8th Ave",
  "addressLine2": "4th Floor",
  "state": "NY",
  "city": "New York",
  "zip": "10011"
}
```
Structured Address:

```json
{
  "company": "Trimble",
  "website": "http://www.trimble.com/",
  "address": {
    "line1": "111 8th Ave",
    "line2": "4th Floor",
    "state": "CA",
    "city": "Sunnyvale",
    "zip": "80242"
  }
}
```

#### Property Name Format

*Choose meaningful property names.*

Property names must conform to the following guidelines:

* Property names should be meaningful, intention revealing names with defined semantics.
* Consistent across the same context. 
* Property names must be camel-cased, ascii strings.
* The first character must be a letter, an underscore (_) or a dollar sign ($).
  * Subsequent characters can be a letter, a digit, an underscore, or a dollar sign.

Avoid:

* Reserved JavaScript keywords
* Acronyms created by the company or division internally.
* Abbreviations
  * Exception: If the abbreviation is universally accepted including: 
	  * Units of Measure (including uom for Unit of Measure).
	  * Industry or Domain acronyms

```javascript
{
  "thisIsAnAcceptablePropertyName": "...",
  "_propertyName": "...",
  "$propertyName": "...",
  "i18PropertyName": "...",
  "someDateUtc": "...", // UTC acronym is universal
  "someId": "..." // Id abbreviation is universal
}
```
Avoid:
```
{
	// Not camel cased.
	"someDateUTC": "...",
	"someID": "..."
	// Starts with digit
	"1TimeUse": "...",
	// snake_case 
	"this_property_is_an_identifier": "...",
	// Division
}
```

#### Key Names in JSON Maps

*JSON maps can use any Unicode character in key names.*

The property name naming rules do not apply when a JSON object is used as a map. A map (also referred to as an associative array) is a data type with arbitrary key/value pairs that use the keys to access the corresponding values. JSON objects and JSON maps look the same at runtime; this distinction is relevant to the design of the API. The API documentation should indicate when JSON objects are used as maps.

The keys of a map do not have to obey the naming guidelines for property names. Map keys may contain any Unicode characters. Clients can access these properties using the square bracket notation familiar for maps (for example, result.thumbnails["72"]).
```javascript
{
  // The "address" property is a sub-object
  // holding the parts of an address.
  "address": {
    "addressLine1": "123 Anystreet",
    "city": "Anytown",
    "state": "XX",
    "zip": "00000"
  },
  // The "thumbnails" property is a map that maps
  // a pixel size to the thumbnail url of that size.
  "thumbnails": {
    "72": "http://url.to.72px.thumbnail",
    "144": "http://url.to.144px.thumbnail"
  }
}
```

#### Reserved Property Names

*Certain property names are reserved for consistent use across services.*

Details about reserved property names, along with the full list, can be found later on in this guide. Services should avoid using these property names for anything other than their defined semantics.

#### Singular vs Plural Names

*Array types should have plural property names. All other property names should be singular.*

There may be exceptions to this, especially when referring to numeric property values. For example, a meta data property for a collection or paged object may have the property totalItems, totalItems makes more sense than totalItem. This is not violating the style guide.
```
{
  // Singular
  "author": "lisa",
  // An array of siblings, plural
  "siblings": [ "bart", "maggie"],
  // This is acceptable
  "totalItems": 10
}
```

#### Property Value Format

*Property values must be Unicode booleans, numbers, strings, objects, arrays, or null.*

The spec at JSON.org specifies exactly what type of data is allowed in a property value. This includes Unicode booleans, numbers, strings, objects, arrays, and null. JavaScript expressions are not allowed. APIs should support that spec for all values, and should choose the data type most appropriate for a particular property (numbers to represent numbers, etc.).

Good:
```
{
  "canPigsFly": null,     // null
  "areWeThereYet": false, // boolean
  "answerToLife": 42,     // number
  "name": "Bart",         // string
  "moreData": {},         // object
  "things": []            // array
}
```
Bad:
```
{
  "aVariableName": aVariableName,         // Bad - JavaScript identifier
  "functionFoo": function() { return 1; } // Bad - JavaScript function
}
```
#### Empty/Null Property Values

*Consider removing empty or null values.*

If a property is optional or has an empty or null value, consider dropping the property from the JSON, unless there's a strong semantic reason for its existence.
```
{
  "volume": 10,

  // Even though the "balance" property's value is zero, it should be left in,
  // since "0" signifies "even balance" (the value could be "-1" for left
  // balance and "+1" for right balance.
  "balance": 0,

  // The "currentlyPlaying" property can be left out since it is null.
  // "currentlyPlaying": null
}
```
#### Enum Values

*Enum values should be represented as strings.*

As APIs grow, enum values may be added, removed or changed. Using strings as enum values ensures that downstream clients can gracefully handle changes to enum values.

Java code:
```
public enum Color {
  WHITE,
  BLACK,
  RED,
  YELLOW,
  BLUE
}
```
JSON object:
```
{
  "color": "WHITE"
}
```
#### Special Value Data Types

As mentioned above, property value types must be booleans, numbers, strings, objects, arrays, or null. However, it is useful define a set of standard data types when dealing with certain values. These data types will always be strings, but they should be formatted in a specific manner so that they can be easily parsed.

#### Date Property Values

*Dates should be formatted as recommended by RFC 3339.*

Dates should be strings formatted as recommended by [RFC 3339](http://www.ietf.org/rfc/rfc3339.txt). RFC 3339 is a profile of [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601).

NOTE: Using ticks should be avoided at all costs. The date/times of ticks are not "human readable" and make the messages harder to understand and debug.
```
{
  "lastUpdate": "2016-04-24T16:34:41.000Z"
}
```
#### Time Duration Property Values

*Time durations should be formatted as recommended by ISO 8601.*

Time duration values should be strings formatted as recommended by [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601#Durations).
```javascript
{
  // three years, six months, four days, twelve hours,
  // thirty minutes, and five seconds
  "duration": "P3Y6M4DT12H30M5S"
}
```
#### Latitude/Longitude/Geographic Feature Property Values

*This should be given special consideration depending on the nature of the data and the targeted client*

* [ISO 6709 - Standard representation of geographic point location by coordinates](https://en.wikipedia.org/wiki/ISO_6709)
* [GeoJSON - format for encoding a variety of geographic data structures](http://geojson.org/geojson-spec.html)
  * as well as [this explanation](http://www.macwright.org/2015/03/23/geojson-second-bite.html)
* Good reference: [lat/lon or lon/lat](http://www.macwright.org/lonlat/)

### Top-Level Meta-Data Properties


| Name | Type | Default | Required | Description |
|------|------|---------|:--------:|-------------|
|kind|string|null||Serves as a namespace to what type of information this particular object stores. It can be present in any object where its helpful to distinguish between various types to help clients parse more effinciently. If the kind property is present, it should be the first property in the object.|
|etag|string|null||Represents the etag for the response. 
|updatedUtc|string|null||Indicates the last date/time (RFC 3339) the item was updated, as defined by the service.|
|deleted|boolean|null||When present, indicates the containing entry is deleted. If deleted is present, its value must be true; a value of false can cause confusion and should be avoided.|

### Collections, Sorting & Paging Property Names


#### Collections

| Name | Type | Default | Required | Description |
|------|------|---------|:--------:|-------------|
|totalItems|numbers|null|X|Number of elements in the result set.|
|items|array|empty|X|The property name items is reserved to represent an array of items. This construct is intended to provide a standard location for collections related to the current result. For example, the JSON output could be plugged into a generic pagination system that knows to page on the items array. If items exists, it should be the last property in the data object.|

```
{
  "totalItems": 7,
  "items": [
    single item,
	single item,
  ]
}
```

#### Sorting

Applied to collections


| Name | Type | Default | Required | Description |
|------|------|---------|:--------:|-------------|
|sort|array||X||
|sort.property|string||X||
|sort.direction|string||X|Allowed values are ASC and DESC
|

```
{
  "sort": [
    {
      "property": "name",
	  "direction": "ASC",	
	}
  ],
  ...
}
```

#### Paging

**By Page Number & Size**

| Name | Type | Default | Required | Description |
|------|------|---------|:--------:|-------------|
|page|number|1|X||
|pageSize|number|10|X||
|itemCount|number||X||

```
{
  "page": 1,
  "pageSize": 10,
  "itemCount": 7,
  ...
}
```

**By Limit & Offset**

| Name | Type | Default | Required | Description |
|------|------|---------|:--------:|-------------|
|limit|number|10|||
|offset|number|0|||
|itemCount|number||||

```
{
  "limit": 10,
  "offset": 0,
  "itemCount": 7,
  ...
}
```

**By Cursor**

Not desirable except for specialized cases such as feeds or batch processes. When this case arises we will update the style guide.

| Name | Type | Default | Required | Description |
|------|------|---------|:--------:|-------------|
|cursor|string||||
|previousCursor|string||||
|nextCursor|string||||
|itemCount|number||||

```
{
  "cursor": "adsflkjh",
  "previousCursor": "zxcvbmbmz",
  "nextCursor": "ouweurwq",
  "itemCount": 7,
  ...
}
```

**By Time**

Not desirable except for specialized cases such as feeds or batch processes. When this case arises we will update the style guide.

| Name | Type | Default | Required | Description |
|------|------|---------|:--------:|-------------|
|limit|number|10|||
|after|number|||Unix timestamp|
|before|number|||Unix timestamp|
|itemCount|number||||

```
{
  "limit": 10,
  "after": 1364587774,
  // and/or
  "before": 1364849754,
  "itemCount": 7,
  ...
}
```


### Error Property Names


| Name | Type | Default | Required | Description |
|------|------|---------|:--------:|-------------|
|status|number||X|Represents the HTTP status code for the error. If there are multiple errors, code will be the error code for the first error.|
|timeStamp|string||X|RFC 3339|
|title|string||X|The title field provides a brief title for the error condition. For example, errors resulting as a result of input validation will have the title “Validation Failure”. Similarly, an “Internal Server Error” will be used for internal server errors.|
|detail|string||X|The detail field contains a short description of the error. The information in this field is typically human-readable and can be presented to an end user. |
|message|string||X|Contains information for the consumer/developer to troubleshoot.|
|errors|array|||The errors field is used to report field validation errors. 1 or more  individual validation errors will be contained within.|
|errors.code|string||X||
|errors.message|string||X||


```
{
  "status": 404,
  "titmeStamp": "",
  "title": "Resource Not Found",
  "detail": "Project with id: 6 not found.",
  "message": "ResourceNotFoundException",
  // Or, if there were multiple errors - such as validation
  "errors": [
    {
      "code": "NotEmpty",
      "message": "name must not be empty."
    }
  ]
  ...
}
```
### Appendix A: JSON Schemas

#### Single Item Schema

```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "id": "http://civilws.trimble.com/v1",
  "type": "object",
  "additionalProperties": true,
  "title": "Item schema.",
  "description": "Describes the schema of a single item",
  "name": "/single",
  "properties": {
    "kind": {
      "id": "http://civilws.trimble.com/v1/kind",
      "type": "string",
      "name": "kind"
    },
    "etag": {
      "id": "http://civilws.trimble.com/v1/etag",
      "type": "string",
      "name": "etag"
    },
    "createdUtc": {
      "id": "http://civilws.trimble.com/v1/createdUtc",
      "type": "string",
      "name": "createdUtc",
    },
    "updatedUtc": {
      "id": "http://civilws.trimble.com/v1/updatedUtc",
      "type": "string",
      "name": "updatedUtc"
    },
    "deleted": {
      "id": "http://civilws.trimble.com/v1/deleted",
      "type": "boolean",
      "name": "deleted"
    }
  },
  "required": []
}
```

#### Collection Schema

```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "id": "http://civilws.trimble.com/v1",
  "type": "object",
  "additionalProperties": true,
  "title": "Collection schema.",
  "description": "An explanation about the purpose of this instance described by this schema.",
  "name": "/collection",
  "properties": {
    "kind": {
      "id": "http://civilws.trimble.com/v1/kind",
      "type": "string",
      "name": "kind"
    },
    "etag": {
      "id": "http://civilws.trimble.com/v1/etag",
      "type": "string",
      "name": "etag"
    },
    "page": {
      "id": "http://civilws.trimble.com/v1/page",
      "type": "integer",
      "minimum": 1,
      "name": "page",
      "default": 1
    },
    "pageSize": {
      "id": "http://civilws.trimble.com/v1/pageSize",
      "type": "integer",
      "minimum": 1,
      "name": "pageSize",
      "default": 10
    },
    "totalPages": {
      "id": "http://civilws.trimble.com/v1/totalPages",
      "type": "integer",
      "minimum": 1,
      "name": "totalPages",
      "default": 1
    },
    "sort": {
      "id": "http://civilws.trimble.com/v1/sort",
      "type": "array",
      "minItems": 1,
      "name": "sort",
      "items": {
        "id": "http://civilws.trimble.com/v1/sort/0",
        "type": "object",
        "name": "0",
        "properties": {
          "property": {
            "id": "http://civilws.trimble.com/v1/sort/0/property",
            "type": "string",
            "minLength": 1,
            "name": "property",
          },
          "direction": {
            "id": "http://civilws.trimble.com/v1/sort/0/direction",
            "type": "string",
            "minLength": 1,
            "name": "direction",
            "default": "ASC",
            "enum": [
              "ASC",
              "DESC"
            ]
          }
        }
      }
    },
    "totalItems": {
      "id": "http://civilws.trimble.com/v1/totalItems",
      "type": "integer",
      "name": "totalItems"
    },
    "items": {
      "id": "http://civilws.trimble.com/v1/items",
      "type": "array",
      "minItems": "0",
      "name": "items",
      "items": {
        "id": "http://civilws.trimble.com/v1/items/0",
        "type": "object",
        "additionalProperties": true,
        "name": "0",
        "properties": {}
      }
    }
  },
  "required": []
}
```

#### Error Schema

```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "id": "http://civilws.trimble.com/v1",
  "type": "object",
  "additionalProperties": true,
  "title": "Error schema.",
  "description": "An explanation about the purpose of this instance described by this schema.",
  "name": "/error",
  "properties": {
    "timeStamp": {
      "id": "http://civilws.trimble.com/v1/timeStamp",
      "type": "string",
      "name": "timeStamp",
    },
    "status": {
      "id": "http://civilws.trimble.com/v1/status",
      "type": "integer",
      "name": "status",
    },
    "title": "[object Object]",
    "description": "[object Object]",
    "message": {
      "id": "http://civilws.trimble.com/v1/message",
      "type": "string",
      "name": "message",
    },
    "errors": {
      "id": "http://civilws.trimble.com/v1/errors",
      "type": "array",
      "minItems": "0",
      "name": "errors",
      "items": {
        "id": "http://civilws.trimble.com/v1/errors/0",
        "type": "object",
        "name": "0",
        "properties": {
          "code": {
            "id": "http://civilws.trimble.com/v1/errors/0/code",
            "type": "string",
            "name": "code",
          },
          "message": {
            "id": "http://civilws.trimble.com/v1/errors/0/message",
            "type": "string",
            "name": "message",
          }
        }
      }
    }
  },
  "required": [
    "timeStamp",
    "status",
    "title",
    "description",
    "message",
    "errors"
  ]
}
```

------------------
Credits & Attributions

This style guide is heavily borrowed from, and in some cases, copied directly from [Google JSON Style Guide](https://google.github.io/styleguide/jsoncstyleguide.xml) which is licensed under [Creative Commons Attribution 3.0 License](http://creativecommons.org/licenses/by/3.0/). Google does not support or endorse this document in any way.
REST API Style Guide
Open with
Matthew Mather 
(matt_mather@trimble.com)Displaying REST API Style Guide.